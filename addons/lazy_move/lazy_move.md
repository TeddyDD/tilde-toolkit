# Lazy Move

Set of utils related to movement and input.

Makes writing simple kinematic character really easy

```gdscript
func _process(delta):
	var d = ui_direction()
	if d.x == 0 and d.y == 0:
		$AnimationPlayer.play("idle")
	else:
		anim_manager.update_animation(d, "walk")
	move_and_slide(ui_direction() * speed)
```

## static ui_wsad_action_setup()

Adds `WSAD` keys to `ui_*` actions (`ui_up` - w etc.)
Call it once.

## static func ui_direction():

Returns normalized vector2 representing pressed `ui_*` actions.
If user is not pressing any keys it returns `Vector2()`

Multiply result by speed and delta any you have simple, player controlled node.

## FourWayAnimationfManager

Helper class useful for top-down characters.
It will play correct animation based on direction/velociy vector.

### Usage

- Create animation player in your node
- Create set of animations prefixed with direction.
   For example: walk -> up_walk, down_walk, left_walk, right_walk
- Create instance of `FourWayAnimationfManager` and pass AnimationPlayer as parameter.

   ```gdscript
   anim_manager = FourWayAnimationManager.new($AnimationPlayer)
   ```
- In `_process` call `update_animation` method providing velociy/direction vector and
   animation name withot prefix.
   
   ```gdscript
   anim_manager.update_animation(direction, "walk")
   ```
   
