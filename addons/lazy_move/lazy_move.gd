extends Reference

# Add WSAD keys to standard ui_* diection actions
# call it once!
static func ui_wsad_action_setup():
	var w = InputEventKey.new()
	w.scancode = KEY_W
	InputMap.action_add_event("ui_up", w)

	var s = InputEventKey.new()
	s.scancode = KEY_S
	InputMap.action_add_event("ui_down", s)

	var d = InputEventKey.new()
	d.scancode = KEY_D
	InputMap.action_add_event("ui_right", d)

	var a = InputEventKey.new()
	a.scancode = KEY_A
	InputMap.action_add_event("ui_left", a)
	
# Returns normalized vector - the direction
# selected by standard ui_* actions
static func ui_direction():
	var result = Vector2()
	if Input.is_action_pressed("ui_up"):
		result.y -= 1
	if Input.is_action_pressed("ui_down"):
		result.y += 1
	if Input.is_action_pressed("ui_left"):
		result.x -= 1
	if Input.is_action_pressed("ui_right"):
		result.x += 1
	result = result.normalized()
	return result

class FourWayAnimationfManager:
	var player

	var _prev_anim = ""
	var _prev_direction = "up"

	func _init(animationPlayer):
		self.player = animationPlayer

	func update_animation(direction_vector, name):
		if abs(direction_vector.x) > abs(direction_vector.y):
			match int(sign(direction_vector.x)):
				1:
					if _prev_anim != name or _prev_direction != "right":
						set_anim("right", name)
				-1:
					if _prev_anim != name or _prev_direction != "left":
						set_anim("left", name)
		elif abs(direction_vector.y) >= abs(direction_vector.x):
			match int(sign(direction_vector.y)):
				1:
					if _prev_anim != name or _prev_direction != "down":
						set_anim("down", name)
				-1:
					if _prev_anim != name or _prev_direction != "up":
						set_anim("up", name)

	func set_anim(direction, name):
		player.play("%s_%s" % [direction, name])
		_prev_direction = direction
		_prev_anim = name