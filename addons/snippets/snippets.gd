extends GDScript

# Returns a 2D array.
static func make_2d_array(vector2, fill=null):
	# Wrong array size
	assert(vector2.x>0 and vector2.y>0)
	var a = []
	for y in range(vector2.y):
		a.append([])
		for x in range(vector2.x):
			a[y].append([])
			a[y][x] = fill
	return a
