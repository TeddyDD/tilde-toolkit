![logo](./icon.png)
# Tilde Toolkit

DRY utils for Godot engine 3.x written to
aid in prototyping games.

## Usage

Each subdirectory contains one module. Read code or docs if provided.
If hard-coded path is required then use:

`res://addons/tilde-toolkit/subdirectory`

## Modules

|name       | description                                                      |
|-----------|------------------------------------------------------------------|
|black_fade |fade in/out to given color. Useful for scene change.              |
|lazy_move  |ui_* actions and movement/input helpers                           |
|snippets   |random util functions. May be moved to other modules in the future|

## License

[MIT License](./LICENSE.txt)
